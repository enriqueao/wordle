import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles: ['./test/setup-tests.ts'],
  testPathIgnorePatterns: ['node_modules', '.d.ts', '.js', './src/util'],
  verbose: true,
  testTimeout: 20000,
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  collectCoverageFrom: ['src/**/*.ts', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  coverageDirectory: 'coverage/apps',
  collectCoverage: true,
  coveragePathIgnorePatterns: ['/tests/', './src/docs', './src/util'],
  coverageThreshold: {
    global: {
      branches: 10,
      functions: 10,
      lines: 10,
      statements: 10,
    },
  },
  passWithNoTests: true,
  moduleNameMapper: {
    '@wordle-api/constants': '<rootDir>/src/constants/index.ts',
    '@wordle-api/common': '<rootDir>/src/common/index.ts',
    '@wordle-api/config/db': '<rootDir>/src/config/db/index.ts',
    '@wordle-api/data-access': '<rootDir>/src/data-access/index.ts',
    '@wordle-api/domain': '<rootDir>/src/domain/index.ts',
    '@wordle-api/handler-errors':
      '<rootDir>/src/platform/lib/class/custom-errors/custom-error.ts',
    '@wordle-api/controllers': '<rootDir>/src/controllers/index.ts',
    '@wordle-api/config/inversify':
      '<rootDir>/src/config/inversify/index.ts',
    '@wordle-api/config/symbols':
      '<rootDir>/src/config/symbols/index.ts',
    '@wordle-api/routes/(.*)': '<rootDir>/src/routes/$1',
    '@wordle-api/platform/(.*)': '<rootDir>/src/platform/$1',
    '@wordle-api/service': '<rootDir>/src/service/index.ts',
    '@wordle-api/interfaces': '<rootDir>/src/interfaces/index.ts',
  },
};

export default config;
