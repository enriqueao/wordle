import { Request, Response } from 'express';
import {
  InitGameUseCase
} from '@wordle-api/domain';
import { InitGameController } from '@wordle-api/controllers';
import { CustomRequest } from '../../../src/platform/server/types';

describe('Controller to init game', () => {
  let mockInitUseCase: InitGameUseCase;
  let defaultMockResponse: unknown;

  const bodyData = { };
  const responseData = {
    userName: "fer",
    nextWord: 300,
    word: "BATEN",
    intents: 0,
    score: 0,
  };
  const mockRequest = {
    body: bodyData,
    requestId: "1",
    dataTokenUser: {
      data: {
        userId: 1,
        userName: 'test'
      }
    }
  } as CustomRequest;

  beforeEach(() => {
    defaultMockResponse = {
      type: jest.fn(),
      status: jest.fn(() => defaultMockResponse),
      sendStatus: jest.fn(),
      json: jest.fn(),
    };
    mockInitUseCase = {
      execute: jest.fn(() => Promise.resolve(responseData)),
    };
  });

  test('should get 201 when initGame data', async () => {
    const initGameController = new InitGameController(
      mockInitUseCase
    );
    const mockResponse = defaultMockResponse as Response;
    await initGameController.execute(mockRequest, mockResponse);
    expect(mockInitUseCase.execute).toHaveBeenCalled();
    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(responseData);
  });

  test('should get 500 Internal Server Error if an unknown error throws by use case', async () => {
    mockInitUseCase.execute = jest.fn(async () =>
      Promise.reject(new Error('JavaScript heap out of memory'))
    );
    const mockResponse = defaultMockResponse as Response;
    const initGameController = new InitGameController(mockInitUseCase);
    await initGameController.execute(mockRequest, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: 'JavaScript heap out of memory',
    });
  });
});
