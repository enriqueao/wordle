import { sql } from '@wordle-api/config/db';
import {
  ScoreRepositoryImpl,
} from '@wordle-api/data-access';

jest.mock('inversify', () => {
  return {
    inject: () => jest.fn(),
    injectable: () => jest.fn(),
  };
});

jest.mock("@wordle-api/config/db", () => {
  return {
    sql: jest.fn(),
  };
});

describe('Database implementation to create a car', () => {
  const spySql = jest.mocked(sql, true);
  let scoreRepositoryImpl: ScoreRepositoryImpl;
  const inputDto = {
    description: "",
    userId: 1,
    score: 0,
    word: "TESTS",
  };

  beforeEach(() => {
    jest.clearAllMocks();
    spySql.mockImplementation(() => Promise.resolve([]));
    scoreRepositoryImpl = new ScoreRepositoryImpl();
  });

  test('should create a score with user data', async () => {
    //spySql.mockImplementation(() => Promise.resolve());
    await scoreRepositoryImpl.create(inputDto);
    const expectedSqlParam = {
      query: `INSERT INTO
        score(scr_usr_act_id, scr_ctg_value, scr_description,scr_score)
        VALUES($1,$2,$3,$4)`,
      bind: [inputDto.userId, inputDto.word, inputDto.description, inputDto.score],
    };
    expect(spySql).toHaveBeenCalledWith(expectedSqlParam);
  });
});
