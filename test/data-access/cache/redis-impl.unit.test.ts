import { getRedisClient } from '@wordle-api/config/db';
import { RedisImpl } from '@wordle-api/data-access';
import { Redis } from 'ioredis';

jest.mock('@wordle-api/config/db', () => {
  return {
    getRedisClient: jest.fn(),
  };
});

describe('Redis cache implementation', () => {
  const redisSpy = jest.mocked(getRedisClient, true);
  let redisImpl: RedisImpl;
  let redisOps: {
    get: jest.Mock;
    set: jest.Mock;
    del: jest.Mock;
  };

  const expectedResponse = {
    id: 1,
    name: 'my cached object',
  };
  const databaseSimulation = async (id: number) => {
    console.info(id);
    return Promise.resolve(expectedResponse);
  };

  beforeEach(() => {
    redisOps = {
      get: jest.fn(),
      set: jest.fn(),
      del: jest.fn(),
    };
    redisSpy.mockClear();
    redisSpy.mockImplementation(() => redisOps as unknown as Redis);
    redisImpl = new RedisImpl();
  });

  test('should return null if no cached data exists by some key', async () => {
    const response = await redisImpl.getByKey('key-to-her-heart');
    expect(response).toBeNull(); // :C
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.get).toHaveBeenCalledWith('key-to-her-heart');
  });

  test('should return a cached value when data exists by some key', async () => {
    redisOps.get = jest.fn(() => Promise.resolve('"DeLorean DMC-12 1981"'));
    const response = await redisImpl.getByKey<string>('my-car-version');
    expect(response).toEqual('DeLorean DMC-12 1981');
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.get).toHaveBeenCalledWith('my-car-version');
  });

  test('should return a cached value that could not parse to JSON', async () => {
    redisOps.get = jest.fn(() => Promise.resolve('Simple String'));
    const response = await redisImpl.getByKey<string>('other-key');
    expect(response).toEqual('Simple String');
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.get).toHaveBeenCalledWith('other-key');
  });

  test('should set in cache some value by key', async () => {
    await redisImpl.setByKey<string>('key', 'value');
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.set).toHaveBeenCalledWith('key', '"value"', 'EX', 20);
  });

  test('should memoize a function value', async () => {
    const responseMemoize = redisImpl.memoize<{ id: number; name: string }>(
      async function getDatabaseSimulation(args) {
        const castedArgs = args as { id: number };
        return await databaseSimulation(castedArgs.id);
      },
      60
    );

    const response = await responseMemoize({ id: 1 });
    expect(response).toStrictEqual(expectedResponse);
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.get).toHaveBeenCalledWith(
      'getDatabaseSimulation-[{"id":1}]'
    );
    expect(redisOps.set).toHaveBeenCalledWith(
      'getDatabaseSimulation-[{"id":1}]',
      expect.stringContaining('my cached object'),
      'EX',
      60
    );
  });

  test('should return a memoize value', async () => {
    redisOps.get = jest.fn(() => Promise.resolve(expectedResponse));
    const responseMemoize = redisImpl.memoize<{ id: number; name: string }>(
      async function getDatabaseSimulation(args) {
        const castedArgs = args as { id: number };
        return await databaseSimulation(castedArgs.id);
      },
      50
    );

    const response = await responseMemoize({ id: 1 });
    expect(response).toStrictEqual(expectedResponse);
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.get).toHaveBeenCalledWith(
      'getDatabaseSimulation-[{"id":1}]'
    );
    expect(redisOps.set).not.toHaveBeenCalled();
  });

  test('should generate a function key without args', () => {
    const response = redisImpl.generateFunctionKey('simple-key');
    expect(response).toEqual('simple-key');
  });

  test('should generate a function key with objects of args', () => {
    const myArgs = { id: 1 };
    const response = redisImpl.generateFunctionKey('simple-key', myArgs);
    expect(response).toEqual('simple-key-{"id":1}');
  });

  test('should delete in cache some value by key', async () => {
    await redisImpl.deleteByKey('key');
    expect(redisSpy).toHaveBeenCalled();
    expect(redisOps.del).toHaveBeenCalledWith('key');
  });
});
