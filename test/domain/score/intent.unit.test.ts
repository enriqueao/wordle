import {
  ScoreRepository,
  ScoreIntent
} from '@wordle-api/domain';
import { config } from '@wordle-api/platform/index';

describe('List notifications', () => {
  let mockScoreRepository: ScoreRepository;
  let mockCacheService: {
    setByKey: jest.Mock;
    getByKey: jest.Mock;
    generateFunctionKey: jest.Mock;
    memoize: jest.Mock;
    deleteByKey: jest.Mock;
    getKeyTLL: jest.Mock;
  };
  let mockQueueService: {
    pub: jest.Mock;
  };
  const inputDto = {
    userWord: "TESTS",
    userId: 1,
    requestId: '1',
    userName: 'test',
  };

  beforeEach(() => {
    mockScoreRepository = {
      create: jest.fn(),
      getTopWords: jest.fn(),
      getTopUsers: jest.fn(),
      getVictoryByUserId: jest.fn(),
      getMatchesByUserId: jest.fn(),
    };
    mockCacheService = {
      setByKey: jest.fn(),
      getByKey: jest.fn(),
      deleteByKey: jest.fn(),
      generateFunctionKey: jest.fn(),
      getKeyTLL: jest.fn(),
      memoize: jest.fn( () => () => Promise.resolve([])),
    };
  });

  test('should create intent when word has been sended then return score', async () => {
    mockCacheService.getByKey = jest.fn(() => Promise.resolve({
      word: '',
      score: '',
      intents: 0
    }))
    const intentUseCase = new ScoreIntent(
      mockScoreRepository,
      mockCacheService
    );
    await intentUseCase.execute(inputDto);
  });

});
