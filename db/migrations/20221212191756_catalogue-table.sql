-- migrate:up
CREATE TABLE catalogue (
	ctg_id int8 NOT NULL DEFAULT nextval('seq_wordle_catalogue_wct_id'::regclass),
	ctg_value varchar(150) NULL,
	ctg_creation_date timestamp NOT NULL DEFAULT now(),
	ctg_update_date timestamp NOT NULL DEFAULT now(),
	ctg_delete_date timestamp NULL,
	CONSTRAINT pk_ctg_id PRIMARY KEY (ctg_id)
);
COMMENT ON COLUMN "catalogue"."ctg_id" IS 'This column represent the id of the entity. It composes the primary key of the table.';
CREATE INDEX ctg_value_idx ON catalogue USING btree (ctg_value);
CREATE INDEX ctg_delete_date_idx ON catalogue USING btree (ctg_delete_date);

-- migrate:down
ALTER TABLE "catalogue" DROP CONSTRAINT "pk_ctg_id";
DROP INDEX "ctg_value_idx";
DROP INDEX "ctg_delete_date_idx";
DROP TABLE "catalogue";
