-- migrate:up
CREATE TABLE score (
	scr_id int8 NOT NULL DEFAULT nextval('seq_wordle_score_wsr_id'::regclass),
	scr_usr_act_id int8 NOT NULL,
	scr_ctg_value varchar(150) NOT NULL,
	scr_description varchar(100) NULL,
	scr_score int8 NOT NULL,
	scr_creation_date timestamp NOT NULL DEFAULT now(),
	scr_update_date timestamp NOT NULL DEFAULT now(),
	scr_delete_date timestamp NULL,
	CONSTRAINT pk_scr_id PRIMARY KEY (scr_id),
	CONSTRAINT fk_usr_id FOREIGN KEY (scr_usr_act_id) REFERENCES user_account(usr_act_id) ON UPDATE CASCADE
);
COMMENT ON COLUMN "score"."scr_id" IS 'This column represent the id of the entity. It composes the primary key of the table.';
CREATE INDEX scr_usr_act_idx ON score USING btree (scr_usr_act_id);
CREATE INDEX scr_delete_date_idx ON score USING btree (scr_delete_date);

-- migrate:down
ALTER TABLE "score" DROP CONSTRAINT "pk_scr_id";
DROP INDEX "scr_usr_act_idx";
DROP INDEX "scr_delete_date_idx";
DROP TABLE "score";
