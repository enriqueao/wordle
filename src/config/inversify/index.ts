import { Container } from 'inversify';
import { SYMBOLS } from '@wordle-api/config/symbols';
import {
  AuthService,
  CacheService,
  IAuthService,
} from '@wordle-api/common';
import { BaseController } from '../../controllers/base-controller';
import {
  ScoreIntentController,
  InitGameController,
  ScoreTopController,
  ScoreStatsController,
  UserSignupController,
  UserSigninController,
} from "@wordle-api/controllers";
import {
  RedisImpl,
  ScoreRepositoryImpl,
  UserRepositoryImpl,
  CatalogueRepositoryImpl,
} from '@wordle-api/data-access';
import {
  ScoreRepository,
  UserRepository,
  CatalogueRepository,
  ScoreIntentUseCase,
  ScoreIntent,
  InitGame,
  InitGameUseCase,
  ScoreTop,
  ScoreTopUseCase,
  ScoreStats,
  ScoreStatsUseCase,
  SignupUseCase,
  Signup,
  SigninUseCase,
  Signin
} from '@wordle-api/domain';


const container = new Container({ defaultScope: 'Singleton' });
container.bind<CacheService>(SYMBOLS.CacheService).to(RedisImpl);
container.bind<IAuthService>(SYMBOLS.AuthService).to(AuthService);

container.bind<BaseController>(BaseController).toSelf();

container
  .bind<ScoreRepository>(SYMBOLS.ScoreRepository)
  .to(ScoreRepositoryImpl);
container
  .bind<UserRepository>(SYMBOLS.UserRepository)
  .to(UserRepositoryImpl);
container
  .bind<CatalogueRepository>(SYMBOLS.CatalogueRepository)
  .to(CatalogueRepositoryImpl);

container.bind<ScoreIntentUseCase>(SYMBOLS.ScoreIntentUseCase)
  .to(ScoreIntent);
container.bind<ScoreIntentController>(ScoreIntentController)
  .toSelf();
container.bind<InitGameUseCase>(SYMBOLS.InitGameUseCase)
  .to(InitGame);
container.bind<InitGameController>(InitGameController)
  .toSelf();
container.bind<ScoreTopUseCase>(SYMBOLS.ScoreTopUseCase)
  .to(ScoreTop);
container.bind<ScoreTopController>(ScoreTopController)
  .toSelf();
container.bind<ScoreStatsUseCase>(SYMBOLS.ScoreStatsUseCase)
  .to(ScoreStats);
container.bind<ScoreStatsController>(ScoreStatsController).toSelf();

container.bind<SignupUseCase>(SYMBOLS.SignupUseCase).to(Signup);
container.bind<UserSignupController>(UserSignupController).toSelf();

container.bind<SigninUseCase>(SYMBOLS.SigninUseCase).to(Signin);
container.bind<UserSigninController>(UserSigninController).toSelf();



export { container };
