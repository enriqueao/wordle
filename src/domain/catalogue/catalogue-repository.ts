export interface CatalogueRepository {
  getWord(exclude: string[]): Promise<string>;
}
