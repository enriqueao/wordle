import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { CacheService } from "@wordle-api/common";
import { ScoreRepository, UseCase, UserRepository } from "@wordle-api/domain";

interface requestStatsDto {
  requestId: string;
  userName: string;
}

interface responseStatsDto {
  victories: number;
  matches: number;
}

export type ScoreStatsUseCase = UseCase<requestStatsDto, responseStatsDto>;

@injectable()
export class ScoreStats implements ScoreStatsUseCase {
  private readonly scoreRepository: ScoreRepository;
  private readonly userRepository: UserRepository;

  public constructor(
    @inject(SYMBOLS.ScoreRepository)
    scoreRepository: ScoreRepository,
    @inject(SYMBOLS.UserRepository)
    userRepository: UserRepository,
  ) {
    this.scoreRepository = scoreRepository;
    this.userRepository = userRepository;
  }

  public async execute(requestDto: requestStatsDto): Promise<responseStatsDto> {
    try {
      const { userName } = requestDto;
      const user = await this.userRepository.getUserByUserName(userName);
      if (!user) {
        throw new Error('user not found')
      }
      const victories = await this.scoreRepository.getVictoryByUserId(
        user.userId
      );
      const matches = await this.scoreRepository.getMatchesByUserId(+user.userId);
      return Promise.resolve({
        victories,
        matches,
      });
    } catch (error) {
      throw new Error(error?.message);
    }
  }
}
