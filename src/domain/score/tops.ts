import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { CacheService } from "@wordle-api/common";
import { ScoreRepository, UseCase } from "@wordle-api/domain";

export interface requestTopDto {
  requestId: string;
}

interface TopWords {
    word: string;
    qty: number;
};
interface TopUsers {
    userName: string;
    userId: number;
    victories: string;
};

export interface responseTopDto {
  words: TopWords[];
  users: TopUsers[];
}

export type ScoreTopUseCase = UseCase<requestTopDto, responseTopDto>;

@injectable()
export class ScoreTop implements ScoreTopUseCase {
  private readonly scoreRepository: ScoreRepository;
  private readonly cacheService: CacheService;

  public constructor(
    @inject(SYMBOLS.ScoreRepository)
    scoreRepository: ScoreRepository,
    @inject(SYMBOLS.CacheService)
    cacheService: CacheService
  ) {
    this.scoreRepository = scoreRepository;
    this.cacheService = cacheService;
  }

  public async execute(requestDto: requestTopDto): Promise<responseTopDto> {
    try {
        const topUsers = await this.scoreRepository.getTopUsers();
        const topWords = await this.scoreRepository.getTopWords();

        return Promise.resolve({
          words: topWords,
          users: topUsers,
        });
    } catch (error) {
      throw new Error(error?.message);
    }
  }
}
