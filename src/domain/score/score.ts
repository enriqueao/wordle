export type Score = {
    id?: number;
    userId: number;
    word: string;
    description: string;
    score: number;
}