import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { CacheService } from "@wordle-api/common";
import { CatalogueRepository, UseCase } from "@wordle-api/domain";
import { redis as redisConstants } from "@wordle-api/constants";

export interface inputInitGameDto {
  userName: string;
  requestId: string;
}

export interface responseInitGameDto {
  userName: string;
  nextWord: number;
  word: string;
  intents: number;
  score: number;
}

export type InitGameUseCase = UseCase<inputInitGameDto, responseInitGameDto>;

const wordsToExcludeKey = 'wordsToExclude';
const FIVE_MINUTES = redisConstants.ONE_MINUTE * 5;

@injectable()
export class InitGame implements InitGameUseCase {
  private readonly catalogueRepository: CatalogueRepository;
  private readonly cacheService: CacheService;

  public constructor(
    @inject(SYMBOLS.CatalogueRepository)
    catalogueRepository: CatalogueRepository,
    @inject(SYMBOLS.CacheService)
    cacheService: CacheService
  ) {
    this.catalogueRepository = catalogueRepository;
    this.cacheService = cacheService;
  }

  public async execute(requestDto: inputInitGameDto): Promise<responseInitGameDto> {
    try {
      const { userName } = requestDto;
      const functionKey = await this.cacheService.generateFunctionKey(
        InitGame.name,
        { userName }
      );

      const isPlaying = await this.cacheService.getByKey(functionKey) as any;

      if(!isPlaying){

        let wordsUsed = (await this.cacheService.getByKey(
          wordsToExcludeKey
        )) as string[];

        const word = await this.catalogueRepository.getWord(wordsUsed);
        const wordCase = word.toUpperCase();
        await this.cacheService.setByKey(
          functionKey,
          { intents: 0, word: wordCase, score: 0 },
          FIVE_MINUTES
        );

        wordsUsed = wordsUsed ? [...wordsUsed, word] : [word];

        await this.cacheService.setByKey(
          wordsToExcludeKey,
          wordsUsed,
          redisConstants.ONE_WEEK
        );

        return Promise.resolve({
          userName,
          nextWord: FIVE_MINUTES,
          word: wordCase,
          intents: 0,
          score: 0,
        });
      }

      const nextWord = await this.cacheService.getKeyTLL(functionKey);

      return Promise.resolve({
        userName,
        nextWord,
        word: isPlaying.word,
        intents: isPlaying.intents,
        score: isPlaying.score
      });
    } catch (error) {
      throw new Error(error?.message);
    }
  }
}
