import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { CacheService } from "@wordle-api/common";
import { ScoreRepository, UseCase } from "@wordle-api/domain";
import { redis as redisConstants, game as gameConstants } from "@wordle-api/constants";

export interface requestDto {
  userWord: string;
  userId: number;
  userName: string;
  requestId: string;
}

export interface responseDto {
  letter: string;
  value: number;
}

export type ScoreIntentUseCase = UseCase<requestDto, responseDto[]>;

@injectable()
export class ScoreIntent implements ScoreIntentUseCase {
  private readonly scoreRepository: ScoreRepository;
  private readonly cacheService: CacheService;

  public constructor(
    @inject(SYMBOLS.ScoreRepository)
    scoreRepository: ScoreRepository,
    @inject(SYMBOLS.CacheService)
    cacheService: CacheService
  ) {
    this.scoreRepository = scoreRepository;
    this.cacheService = cacheService;
  }

  public async execute(requestDto: requestDto): Promise<responseDto[]> {
    const { userWord, userName, userId } = requestDto;
    try {
      const wordCase = userWord.toUpperCase();
      const userWordIntent = Array(...wordCase);

      if (userWordIntent.length !== 5) {
        throw new Error(gameConstants.messages.wordInvalid);
      }
      const functionKey = await this.cacheService.generateFunctionKey( "InitGame", { userName } );

      const isPlaying = (await this.cacheService.getByKey(functionKey)) as any;
      if (!isPlaying) {
        throw new Error(gameConstants.messages.initGameRequired);
      }

      const wordGoal = isPlaying.word;
      const actualScore = isPlaying.score;
      const results = [];

      let numIntents = isPlaying.intents;
      if (numIntents === 5) {
        await Promise.all([
          this.cacheService.deleteByKey(functionKey),
          this.scoreRepository.create({
            userId,
            score: isPlaying.score,
            description: gameConstants.status.gameOver,
            word: wordGoal,
          }),
        ]);

        throw new Error(gameConstants.messages.gameOver);
      }

      userWordIntent.forEach((letter, index) => {
        if (wordGoal[index] === letter) {
          results.push({
            letter,
            value: 1,
          });
        } else if (wordGoal.includes(letter)) {
          results.push({
            letter,
            value: 2,
          });
        } else {
          results.push({
            letter,
            value: 3,
          });
        }
      });

      const wordScore = results.reduce((acc, obj) => acc + obj.value, 0);
      const totalScore = actualScore + wordScore;

      if (wordCase === wordGoal) {
        await Promise.all([
          this.scoreRepository.create({
            userId: 1,
            score: totalScore,
            description: gameConstants.status.victory,
            word: wordGoal,
          }),
          this.cacheService.deleteByKey(functionKey),
        ]);

        return Promise.resolve(results);
      }

      numIntents++;
      const remain = await this.cacheService.getKeyTLL(functionKey);
      await this.cacheService.setByKey(
        functionKey,
        { intents: numIntents, word: wordGoal, score: totalScore },
        remain
      );

      return Promise.resolve(results);
    } catch (error) {
      throw new Error(error?.message);
    }
  }
}
