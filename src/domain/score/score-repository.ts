import { Score } from "./score";

export interface ScoreRepository {
  create(data: Score): Promise<void>;
  getTopWords(): Promise<any[]>;
  getTopUsers(): Promise<any[]>;
  getVictoryByUserId(userId: number): Promise<number>;
  getMatchesByUserId(userId: number): Promise<number>;
}
