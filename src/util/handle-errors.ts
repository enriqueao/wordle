import { readFileSync } from 'fs';
import * as path from 'path';
import { isObject } from 'lodash';
import { error as logError, warning as logWarning } from '@kavak/log-node-sdk';

import { backend, selectLanguageBackend } from '@wordle-api/constants';

import {
  IErrorMessage,
  ICustomError,
  EnumAlertLevel,
  ICustomErrorBatch,
} from '../interfaces/backend-error-messages.interface';
import {
  CustomError,
  CustomWarning,
  CustomWarningBatch,
} from '../platform/lib/class/custom-errors';

const getErrorOrDefault = async (
  code: string,
  errorsInLanguage: { [x: string]: any }
) => {
  const error = errorsInLanguage[code];
  const genericError = backend.i18n.errorCodes.genericMessageErrorCode;
  if ((!error || !error?.message) && process.env?.ERROR_CODES) {
    const pathFile = path.join(process.cwd(), process.env?.ERROR_CODES);
    const templateString = await readFileSync(pathFile, 'utf8');
    const localErrors = JSON.parse(templateString);
    return localErrors[code] || localErrors[genericError];
  }
  return error || errorsInLanguage[genericError];
};

const isErrorCodeAllowed = (error: ICustomError) =>
  backend.allowedErrors.includes(error.code);

class ReturnError extends Error {
  originalError;
  code;
  constructor(
    message: string | undefined,
    originalError: ICustomError,
    stage = 'master' /* Backward compatibility pre wrappers */
  ) {
    super(message);
    if (['dev', ...backend.nonProductiveEnvironments].includes(stage)) {
      this.originalError = {
        message: originalError.message,
        code: originalError.code,
        name: originalError.name,
        ...originalError.vars,
        stack: originalError.stack,
      };
    }

    if (isErrorCodeAllowed(originalError)) {
      this.code = originalError.code;
    }
  }
}
const loggingConsoleBehavior = (
  error: ICustomError | ICustomErrorBatch,
  message: string
) => {
  if (error.alertLevel === EnumAlertLevel.LOW) {
    logWarning(message, error);
  } else {
    logError(message, error);
  }
};

const setErrorCodes = (error: ICustomError | ICustomErrorBatch): string[] => {
  let codes: string[] = [];
  let code: string;
  const GENERIC_MESSAGE_ERROR_CODE =
    backend.i18n.errorCodes.genericMessageErrorCode;
  // Codes catalogue by error instance
  if (error instanceof CustomWarningBatch && error.codes) {
    codes = error.codes;
  } else if (
    (error instanceof CustomError || error instanceof CustomWarning) &&
    error.code
  ) {
    code = error.code;
    codes.push(code);
  } else if (error instanceof Error) {
    code = GENERIC_MESSAGE_ERROR_CODE;
    codes.push(code);
  }
  return codes;
};

const setInterpolatedValues = (
  error: ICustomError | ICustomErrorBatch,
  message: string,
  index: number
) => {
  // Replace content in errors
  const isArray = Array.isArray(error.vars);
  const interpolatedVariables = isArray ? error.vars[index] : error.vars;
  if (isObject(interpolatedVariables)) {
    const keys = Object.keys(interpolatedVariables);
    for (const key of keys) {
      const regex = new RegExp(`{${key}}`, 'g');
      message = message.replace(regex, interpolatedVariables[key]);
    }
  }
  return message;
};

const setErrorMessage = async (
  error: ICustomError | ICustomErrorBatch,
  code: string,
  errorsInLanguage: any,
  index: number
) => {
  const i18nError = (await getErrorOrDefault(
    code,
    errorsInLanguage
  )) as IErrorMessage;
  let message = `${i18nError.message}`;
  message = setInterpolatedValues(error, message, index);
  return message;
};

export const handleErrors = async (
  error: ICustomError | ICustomErrorBatch,
  countryId?: number,
  stage?: string | undefined,
  span?: any
) => {
  let codes: string[] = [];
  let message = error.message;
  try {
    // Get language
    const language = selectLanguageBackend(countryId);
    codes = setErrorCodes(error);
    if (codes.length) {
      const messages = await Promise.all(
        codes.map(async (code, index) => {
          return setErrorMessage(error, code, language, index);
        })
      );
      message = messages.join('\n');
    }
  } catch (e) {
    console.error('Error catch', JSON.stringify(e));
    let codeBrokeError = '';
    if (error instanceof CustomWarningBatch) {
      codeBrokeError = error.codes.join('\n');
    } else if (
      error instanceof CustomWarning ||
      error instanceof CustomWarning
    ) {
      codeBrokeError = error.code;
    }
    const errorMessage = `Error: ${e.message} - trying to get ${
      error.name
    } with code ${codeBrokeError || null}`;
    e.message = errorMessage;
    message = errorMessage;
  } finally {
    const shouldMonitorError =
      (typeof error.monitor === 'undefined' || error.monitor) &&
      !process.env.IS_OFFLINE;
    if (shouldMonitorError) {
      span?.setTag('error', error);
    } else {
      span?.setTag('no_monitor_error', error);
    }
    loggingConsoleBehavior(error, message);
  }
  return new ReturnError(message, error as ICustomError, stage);
};
