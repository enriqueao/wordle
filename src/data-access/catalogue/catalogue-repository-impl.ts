import { injectable } from 'inversify';
import { sql } from '@wordle-api/config/db';
import { CatalogueRepository } from "@wordle-api/domain";


@injectable()
export class CatalogueRepositoryImpl implements CatalogueRepository {
  async getWord(exclude): Promise<string> {
    const results = await sql({
      query:
        `SELECT *
        FROM catalogue
        WHERE ctg_value NOT IN ($1) AND LENGTH(ctg_value) = 5 order BY random() LIMIT 1;
      `,
      bind: [exclude]
    }) as unknown as any;

    if(results){
      return results.rows[0] ? results.rows[0].ctg_value : "";
    }

    return results.rows;
  }
}
