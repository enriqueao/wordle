export const INSERT_EVENT = `
  INSERT INTO event (
    evt_tpc_id,
    evt_ety_id,
    evt_chn_id,
    evt_cta_code,
    evt_payload_data,
    evt_transaction_id,
    evt_country_id,
    evt_source
  )
  VALUES($1, $2, $3, $4, $5, $6, $7, $8)
`;

export const SELECT_CHANNELS = `
  SELECT
    chn_id as id,
    chn_name as name,
    chn_code as code,
    chn_description as description
  FROM channel
  WHERE chn_is_enabled = TRUE;
`;

export const SELECT_TOPICS = `
  SELECT
    tpc_id as id,
    tpc_name as name,
    tpc_code as code,
    tpc_icon as icon,
    tpc_description as description
  FROM topic;
`;

export const SELECT_EVENT_TYPES = `
  SELECT *
  FROM event_type;
`;

export const CLOSE_EVENT_BY_TRANSACTION = `
  UPDATE event
  SET evt_delete_date = NOW()
  WHERE evt_delete_date IS NULL
    AND evt_transaction_id = $1
    AND evt_chn_id = $2
    AND evt_tpc_id = $3
`;

export const SELECT_RECEIVER_EVENT = `
  SELECT
    evt_id,
    n.ntf_receiver_id as receiver_id
  FROM event
  INNER JOIN notification n
      ON evt_id = n.ntf_evt_id
  WHERE evt_transaction_id = $1
    AND evt_chn_id = $2
    AND evt_tpc_id = $3
`;
