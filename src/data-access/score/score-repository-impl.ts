import { injectable } from 'inversify';
import { sql } from '@wordle-api/config/db';
import { ScoreRepository } from '@wordle-api/domain';
import { Score } from '../../domain/score/score';


@injectable()
export class ScoreRepositoryImpl implements ScoreRepository {
  async create(data: Score): Promise<void> {
    const { description, score, userId, word } = data;
    await sql({
      query: `INSERT INTO
        score(scr_usr_act_id, scr_ctg_value, scr_description,scr_score)
        VALUES($1,$2,$3,$4)`,
      bind: [userId, word, description, score],
    });
  }

  async getTopWords(): Promise<any[]> {
    const results = (await sql({
      query: `
        SELECT COUNT(*) as qty, scr_ctg_value as word
        FROM score
        WHERE scr_description = 'VICTORY'
        GROUP BY scr_ctg_value
        LIMIT 10;
      `,
    })) as any;
    return results.rows;
  }

  async getTopUsers(): Promise<any[]> {
    const results = (await sql({
      query: `
        SELECT COUNT(*) as victories, scr_usr_act_id as "userId", ua.usr_act_name as "userName"
        FROM score
        INNER JOIN user_account ua
          ON scr_usr_act_id = ua.usr_act_id
        WHERE scr_description = 'VICTORY'
        GROUP BY scr_usr_act_id, ua.usr_act_name
        ORDER BY victories
        LIMIT 10
      `,
    })) as any;
    return results.rows;
  }

  async getVictoryByUserId(userId: number): Promise<number> {
    const results = await sql({
      query: `
        SELECT COUNT(*) as victories
        FROM score
        where scr_usr_act_id = $1 and scr_description = 'VICTORY'
        group by scr_usr_act_id
      `,
      bind: [userId],
    }) as any;
    if (results) {
      return results.rows[0] ? results.rows[0].victories : 0;
    }

    return 0;
  }

  async getMatchesByUserId(userId: number): Promise<number> {
    const results = await sql({
      query: `
        SELECT COUNT(*) as matches
        FROM score
        where scr_usr_act_id = $1
        group by scr_usr_act_id
      `,
      bind: [userId],
    }) as any;

    if (results) {
      return results.rows[0] ? results.rows[0].matches : 0;
    }

    return 0;
  }
}
