export type ScoreDb = {
  id: number;
  name: string;
  description: string;
  code: string;
}
