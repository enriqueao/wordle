import { Router } from 'express';
import { container } from '@wordle-api/config/inversify';
import {
  ScoreIntentController,
  InitGameController,
  ScoreTopController,
  ScoreStatsController,
} from "@wordle-api/controllers";
import { validateToken } from '@wordle-api/platform/middlewares/validate-token';


const scoreIntent = container.get(ScoreIntentController);
const initGame = container.get(InitGameController);
const scoreTops = container.get(ScoreTopController);
const scoreStats = container.get(ScoreStatsController);

const scoreRouter = Router();

scoreRouter.post(
  '/intent',
  validateToken,
  (req, res) => scoreIntent.execute(req, res)
);

scoreRouter.post(
  '/start',
  validateToken,
  (req, res) => initGame.execute(req, res)
);

scoreRouter.get(
  '/tops',
  validateToken,
  (req, res) => scoreTops.execute(req, res)
);

scoreRouter.get(
  '/stats',
  validateToken,
  (req, res) => scoreStats.execute(req, res)
);

export { scoreRouter };
