import { Router } from 'express';
import { scoreRouter } from "./score";
import { userRouter } from './user';

const v1Routes = Router();

v1Routes.use("/v1/score", scoreRouter);
v1Routes.use("/v1/user", userRouter);

export { v1Routes };
