import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { Response } from "express";
import { constants } from "http2";

import { ScoreStatsUseCase } from "@wordle-api/domain";
import { CustomRequest } from "@wordle-api/platform/server/types";
import { CustomError } from "@wordle-api/platform/lib/class/general-error";
import { BaseController } from "../base-controller";

const {
  HTTP_STATUS_CREATED,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
} = constants;

@injectable()
export class ScoreStatsController extends BaseController {
  private scoreStats: ScoreStatsUseCase;

  public constructor(
    @inject(SYMBOLS.ScoreStatsUseCase)
    scoreStats: ScoreStatsUseCase
  ) {
    super();
    this.scoreStats = scoreStats;
  }

  async execute(request: CustomRequest, response: Response): Promise<Response> {
    const { body, dataTokenUser } = request;

    const inputDto = {
      ...body,
      requestId: request.requestId,
    };

    try {
        if(!body.userName){
            throw new Error('userName missing in body');
        }

      // Permitirá obtener cuantas partidas a jugado un usuario y cuantas victorias ha tenido
      const dataDto = await this.scoreStats.execute(inputDto);

      return this.ok(request, response, HTTP_STATUS_CREATED, dataDto);
    } catch (error) {
      return this.fail(
        request,
        response,
        HTTP_STATUS_INTERNAL_SERVER_ERROR,
        error
      );
    }
  }
}
