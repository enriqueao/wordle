import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { Response } from "express";
import { constants } from "http2";

import { InitGameUseCase } from "@wordle-api/domain";
import { CustomRequest } from "@wordle-api/platform/server/types";
import { CustomError } from "@wordle-api/platform/lib/class/general-error";
import { BaseController } from "../base-controller";

const {
  HTTP_STATUS_CREATED,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
} = constants;

@injectable()
export class InitGameController extends BaseController {
  private initGame: InitGameUseCase;

  public constructor(
    @inject(SYMBOLS.InitGameUseCase)
    initGame: InitGameUseCase
  ) {
    super();
    this.initGame = initGame;
  }

  async execute(request: CustomRequest, response: Response): Promise<Response> {
    const { body, dataTokenUser } = request;

    const inputDto = {
      ...body,
      requestId: request.requestId,
      userName: dataTokenUser.data.userName,
      userId: dataTokenUser.data.userId,
    };

    try {
      const dataDto = await this.initGame.execute(inputDto);
      return this.ok(request, response, HTTP_STATUS_CREATED, dataDto);
    } catch (error) {
      return this.fail(
        request,
        response,
        HTTP_STATUS_INTERNAL_SERVER_ERROR,
        error
      );
    }
  }
}
