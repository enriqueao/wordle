import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { Response } from "express";
import { constants } from "http2";

import { ScoreTopUseCase } from "@wordle-api/domain";
import { CustomRequest } from "@wordle-api/platform/server/types";
import { CustomError } from "@wordle-api/platform/lib/class/general-error";
import { BaseController } from "../base-controller";

const {
  HTTP_STATUS_CREATED,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
} = constants;

@injectable()
export class ScoreTopController extends BaseController {
  private scoreTop: ScoreTopUseCase;

  public constructor(
    @inject(SYMBOLS.ScoreTopUseCase)
    scoreTop: ScoreTopUseCase
  ) {
    super();
    this.scoreTop = scoreTop;
  }

  async execute(request: CustomRequest, response: Response): Promise<Response> {
    const { body } = request;

    const inputDto = {
      ...body,
      requestId: request.requestId,
    };

    try {
      // Permitirá obtener los mejores 10 jugadores con su número de victorias
      // Permitirá obtener las palabras más acertadas
      const dataDto = await this.scoreTop.execute(inputDto);

      return this.ok(request, response, HTTP_STATUS_CREATED, dataDto);
    } catch (error) {
      return this.fail(
        request,
        response,
        HTTP_STATUS_INTERNAL_SERVER_ERROR,
        error
      );
    }
  }
}
