import { inject, injectable } from "inversify";
import { SYMBOLS } from "@wordle-api/config/symbols";
import { Response } from "express";
import { constants } from "http2";

import { ScoreIntentUseCase } from "@wordle-api/domain";
import { CustomRequest } from "@wordle-api/platform/server/types";
import { CustomError } from "@wordle-api/platform/lib/class/general-error";
import { BaseController } from "../base-controller";

const {
  HTTP_STATUS_CREATED,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
} = constants;

@injectable()
export class ScoreIntentController extends BaseController {
  private scoreIntent: ScoreIntentUseCase;

  public constructor(
    @inject(SYMBOLS.ScoreIntentUseCase) scoreIntent: ScoreIntentUseCase
  ) {
    super();
    this.scoreIntent = scoreIntent;
  }

  async execute(request: CustomRequest, response: Response): Promise<Response> {
    const { user_word } = request.body;
    const { dataTokenUser } = request;

    const inputDto = {
      requestId: request.requestId,
      userName: dataTokenUser.data.userName,
      userId: dataTokenUser.data.userId,
      userWord: user_word,
    };

    try {
      const dataDto = await this.scoreIntent.execute(inputDto);

      return this.ok(request, response, HTTP_STATUS_CREATED, dataDto);
    } catch (error) {
      return this.fail(
        request,
        response,
        HTTP_STATUS_INTERNAL_SERVER_ERROR,
        error
      );
    }
  }
}
