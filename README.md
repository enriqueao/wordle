# Wordle API
## Stack Used
* Redis
* Postgress
* TypeScript
* Docker
* Clean Architecture
* Dependency Injection

you need create a env file called `.env.local` in project root with
```
# App env
PORT=8080
HOST=0.0.0.0

# Database
DB_HOST=database
DB_PORT=5433
DB_USERNAME=username
DB_PASSWORD=password
DB_NAME=wordle-db

# Cache
REDIS_HOST=cache
REDIS_PORT=6380
REDIS_PASSWORD=password
```

## Host

local: http://localhost:8080
can use Insomia collection `Insomnia_2022-12-14.json` inside of docs folder

## Run Project
``` sh
> npm run dev
```

## Dbmate
Dbmate to run migrations
```sh
> export DATABASE_URL="postgres://username:password@localhost:5433/wordle-db?sslmode=disable"
> npx dbmate up
```

## Auth

header

`access-token`: ...token

see sign-up and sign-up

## Endpoints

### SignUp

`POST` /v1/user/signup
Request
```json
{
    "userName": "enrique",
    "password": "1234"
}
```
Response
```json
{
	"userName": "enrique"
}
```
### SignIn

`POST` /v1/user/signin
Request
```json
{
    "userName": "enrique",
    "password": "1234"
}
```
Response
you can take token from headers as access-token
```json
{
	"token": ...token
}
```


### Init Game

`POST` /v1/score/start
> Need Access Token
Request
```json
{ }
```
Response
```json
{
	"userName": "enrique",
	"nextWord": 293,
	"word": "BRECÉ",
	"intents": 0,
	"score": 0
}
```
### Sent Word Intent

`POST` /v1/score/intent
> Need Access Token

Request
```json
{
	"user_word": "BRECÉ"
}
```
Response
```json
[
	{
		"letter": "B",
		"value": 1
	},
	{
		"letter": "R",
		"value": 1
	},
	{
		"letter": "E",
		"value": 1
	},
	{
		"letter": "C",
		"value": 1
	},
	{
		"letter": "É",
		"value": 1
	}
]
```

### Score Tops

`GET` /v1/score/tops
> Need Access Token

Request
```json
{ }
```
Response
```json
{
	"words": [
		{
			"qty": "1",
			"word": "BRECÉ"
		},
		{
			"qty": "1",
			"word": "CODEÓ"
		}
	],
	"users": [
		{
			"victories": "2",
			"userId": "1",
			"userName": "enrique"
		}
	]
}
```
### Game Stats by User

`GET` /v1/score/stats
> Need Access Token

Request
```json
{
	"userName": "enrique"
}
```
Response
```json
{
	"victories": "2",
	"matches": "2"
}
```

## Test Coverage (Global 50%)

![Coverage](./docs/images/test_coverage.png)